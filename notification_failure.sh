#!/bin/bash
# SLACK INTEGRATION
curl -X POST -H 'Content-type: application/json' --data '{"text":"Ocorreu um erro na pipeline :("}' https://hooks.slack.com/services/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

# ROCKETCHAT INTEGRATION
curl -X POST -H 'Content-Type: application/json' --data '{"text":"Tudo certo :)","attachments":[{"title":"[ FALHA ] Pipeline executada com erros","title_link":"https://rocket.chat","text":"Corrigir os erros correspondentes","color":"red"}]}' http://localhost/hooks/ThqyPxrpt4NQBF6bu/9M655SuMxMcmKCSeZnsJXKHy9PztssMogoDAJouQNocfecXz