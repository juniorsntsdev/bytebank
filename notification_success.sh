#!/bin/bash
# Notification SLACK
curl -X POST -H 'Content-type: application/json' --data '{"text":"Tudo ocorreu certo na pipeline :)"}' https://hooks.slack.com/services/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

# Notification ROCKETCHAT
curl -X POST -H 'Content-Type: application/json' --data '{"text":"Tudo certo :)","attachments":[{"title":"[ SUCESSO ] Pipeline executada com sucesso","title_link":"https://rocket.chat","text":"Verifique o resultado em: http://localhost:8081","color":"#764FA5"}]}' http://localhost/hooks/ThqyPxrpt4NQBF6bu/9M655SuMxMcmKCSeZnsJXKHy9PztssMogoDAJouQNocfecXz